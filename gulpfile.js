var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var concat = require('gulp-concat');

var PATHS = {
  sass: [
    './node_modules/bootstrap-sass/assets/stylesheets',
  ],
  angularjs: './node_modules/angular/angular.min.js',
  angularRoute: './node_modules/angular-route/angular-route.min.js',
  bootstrapjs: './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
  jquery: './node_modules/jquery/dist/jquery.min.js',
  angularAnimate: './node_modules/angular-animate/angular-animate.min.js',
}

gulp.task('sass', function() {
  return gulp.src('scss/**/*.scss')
  .pipe(sass({
      includePaths: PATHS.sass
    }))
    .pipe(concat('styles.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('css/'))
});


gulp.task('js', function() {
  return gulp.src([PATHS.angularjs, PATHS.angularRoute, PATHS.angularAnimate, PATHS.jquery, PATHS.bootstrapjs,'js/scripts.js'])
  .pipe(concat('main.js'))
    .pipe(gulp.dest('js/'))
});
