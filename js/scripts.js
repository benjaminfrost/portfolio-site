$(document).ready(function(){

  $(".nav li").on("click", function() {
    $(".nav li").removeClass("active");
    $(this).addClass("active");
  });

  $(".navbar-brand").on("click", function() {
    $(".nav li").removeClass("active");
  });

  $(document).on('show','.accordion', function (e) {

       $(e.target).prev('.accordion-heading').addClass('accordion-active');
  });

  $(document).on('hide','.accordion', function (e) {
      $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-active');

  });


});


var app = angular.module("portfolio", ["ngRoute", "ngAnimate"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/home.htm"
    })
    .when("/about", {
        templateUrl : "views/about.htm"
    })
    .when("/examples", {
        templateUrl : "views/examples.htm"
    })
    .when("/contact", {
        templateUrl : "views/contact.htm"
    })
});
